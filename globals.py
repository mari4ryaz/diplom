import os
ROOT_DIR = os.path.dirname(os.path.abspath(__file__))
PARSER_PATH = ROOT_DIR
DATA_PATH = f'{ROOT_DIR}\DATA\\'
TEMP_PATH = f'{ROOT_DIR}\TEMP\\'
UBUNTU_FLAG = 1